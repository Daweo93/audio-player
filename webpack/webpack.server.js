const merge = require('webpack-merge');
const dev = require('./webpack.dev.js');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const browserSync = new BrowserSyncPlugin(
  {
    host: 'localhost',
    port: 5500,
    server: {
      baseDir: ['public/']
    }
  },
  {
    injectCss: true
  }
);

module.exports = merge(dev, {
  plugins: [browserSync]
});
