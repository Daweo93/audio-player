export function validateCalback (callback) {
  if (typeof callback !== "function") {
    return new Error("Callback should be a function");
  }
}