import "../css/style.scss";
import { requireAll } from "./utils/requireAll";
import Visualiser from "./Visualiser";
import Audio from "./Audio";
import File from "./File";

requireAll(require.context("../", true, /^.\/[a-z]+\/\w+\.(pug|html)$/));

window.onload = () => {
  const fileEl = document.querySelector(".js-file");
  const audioEl = document.querySelector(".js-audio");
  const canvasEl = document.querySelector(".js-canvas");
  const infoEl = document.querySelector(".js-info");
  const visualiser = new Visualiser(audioEl, canvasEl);
  const audio = new Audio(audioEl);
  const file = new File(fileEl, infoEl);

  audio.play(visualiser.startVisualization);
  audio.pause(visualiser.stopVisualization);
  file.change(visualiser.setAudio);
};
