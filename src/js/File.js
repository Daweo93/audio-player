import { validateCalback } from "./utils/validation";

export default class File {
  constructor(fileEl, infoEl) {
    this.fileEl = fileEl;
    this.infoEl = infoEl;
  }

  change = callback => {
    this.fileEl.addEventListener("change", event => {
      validateCalback(callback);

      const file = event.target.files[0];

      if (file.type.match("^audio/(mp3|ogg|mpeg|wav)$")) {
        this.infoEl.innerText = file.name;
        callback(file);
      } else {
        this.infoEl.innerText = "Selected extension is not supported.";
      }
    });
  };
}
