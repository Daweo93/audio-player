import { validateCalback } from "./utils/validation";

export default class Audio {
  constructor(audioEl) {
    this.audioEl = audioEl;
  }

  play = callback => {
    this.audioEl.addEventListener("play", () => {
      validateCalback(callback);

      return callback();
    });
  };

  pause = callback => {
    this.audioEl.addEventListener("pause", () => {
      validateCalback(callback);

      return callback();
    });
  };
}
