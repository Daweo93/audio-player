export default class Visualiser {
  constructor(audio, canvas) {
    this.audioEl = audio;
    this.canvasEl = canvas;
    this.context = new AudioContext();
    this.analyser = this.context.createAnalyser();
    this.audioSrc = this.context.createMediaElementSource(this.audioEl);
    this.analyser.fftSize = 256;
    this.canvasEl.width = window.innerWidth;
    this.canvasEl.height = 300;
    this.canvasCtx = this.canvasEl.getContext("2d");
    this.WIDTH = this.canvasEl.width;
    this.HEIGHT = this.canvasEl.height;
  }

  setAudio = file => {
    this.file = file;
    this.audioEl.src = URL.createObjectURL(file);
    this.audioSrc.connect(this.analyser);
    this.analyser.connect(this.context.destination);
    this.bufferLength = this.analyser.frequencyBinCount;
    this.dataArray = new Uint8Array(this.bufferLength);
    this.barWidth = this.WIDTH / this.bufferLength * 2.5;
  };

  renderFrame = () => {
    this.visualization = requestAnimationFrame(this.renderFrame);
    this.analyser.getByteFrequencyData(this.dataArray);
    this.canvasCtx.fillStyle = "#000";
    this.canvasCtx.fillRect(0, 0, this.WIDTH, this.HEIGHT);
    let x = 0;

    for (let i = 0; i < this.bufferLength; i++) {
      const barHeight = this.dataArray[i];
      const r = 50;
      const g = 250 * (i / this.bufferLength);
      const b = barHeight + 25 * (i / this.bufferLength);

      this.canvasCtx.fillStyle = `rgb(${r}, ${g}, ${b})`;
      this.canvasCtx.fillRect(
        x,
        this.HEIGHT - barHeight,
        this.barWidth,
        barHeight
      );

      x += this.barWidth + 1;
    }
  };

  startVisualization = () => {
    if (!this.file) {
      return;
    }

    this.renderFrame();
  };

  stopVisualization = () => {
    if (!this.file) {
      return;
    }

    this.clearBars();

    setTimeout(() => {
      cancelAnimationFrame(this.visualization);
    }, 5000);
  };

  clearBars = () => {
    this.dataArray = this.dataArray.map(() => 0);
  };
}
